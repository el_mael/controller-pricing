import sqlite3
import sys
from getopt import getopt as geto
from mccb import main as mccb
from mcb import main as mcb


conn = sqlite3.connect('parts.sqlite')
cur = conn.cursor()

opts, args = geto(sys.argv[1:], 'p:o:v:i:d:t:n:')

parts=None
pole=None
volts=None
current=None
description=None
terminals=None
itemName=None

for opt, arg in opts:
	if opt == '-p': parts=arg
	elif opt == '-o': pole=arg
	elif opt == '-v': volts=arg
	elif opt == '-i': current=arg
	elif opt == '-d': description=arg
	elif opt == '-t': terminals=arg
	elif opt == '-n': itemName=arg


match parts:
	case "mccb":
		if itemName is None:price = mccb(pole=pole, volts=volts, current=current, conn=conn, cur=cur)
		else:price = mccb(itemName=itemName, conn=conn, cur=cur)
		try: print(f'{price:,}') 
		except: pass
	case "mcb":
		if itemName is None:price = mcb(pole=pole,volts=volts, current=current, conn=conn, cur=cur)
		else:price = mcb(itemName=itemName, conn=conn, cur=cur)
		try: print(f'{price:,}') 
		except: pass
	case "contactor":
		contactor(pole, volts, current, conn=conn, cur=cur)
	case "terminal":
		terminal(terminals, conn=conn, cur=cur)
	case "thermal":
		thermal(current, conn=conn, cur=cur)
	case "others":
		others(description, conn=conn, cur=cur)
